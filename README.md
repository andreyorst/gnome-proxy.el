# gnome-proxy.el

Small utility function to set up proxy environment variables based on GNOME settings.

``` emacs-lisp
(require 'gnome-proxy)

(add-hook 'after-init-hook #'gnome-proxy-setup-env)
```
