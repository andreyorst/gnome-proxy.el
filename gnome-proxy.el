;;; gnome-proxy.el --- Configure proxy env variables based on GNOME system settings -*- lexical-binding: t -*-
;;
;; Author: Andrey Listopadov
;; Homepage: https://gitlab.com/andreyorst/gnome-proxy.el
;; Package-Requires: ((emacs "26.1") (gsettings))
;; Keywords: tools
;; Prefix: gnome-proxy
;; Version: 0.0.2
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with gnome-proxy.el.  If not, see <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; `gnome-proxy-setup-env' is a simple command that sets up the
;; environment variables based on system settings in GNOME.  It relies
;; on the `gsettings' package, which requires the gsettings executable
;; to be present on the system.
;;
;; `gnome-proxy-watch-mode' is a global minor mode that monitors the
;; state of the proxy configuration via the dconf watch process.  When
;; changed, it calls `gnome-proxy-setup-env'.
;;
;;; Code:

(require 'gsettings)

(defun gnome-proxy--get-host (scheme)
  "Get host for SCHEME."
  (let ((host (gsettings-get
               (format "org.gnome.system.proxy.%s" scheme)
               "host")))
    (and (not (string-empty-p host)) host)))

(defun gnome-proxy--get-port (scheme)
  "Get port for SCHEME."
  (let ((port (gsettings-get
               (format "org.gnome.system.proxy.%s" scheme)
               "port")))
    (and (numberp port) port)))

(defun gnome-proxy--get-ignore ()
  "Get ignore hosts setting."
  (let ((ignore (gsettings-get
                 "org.gnome.system.proxy"
                 "ignore-hosts")))
    (and (vectorp ignore) (string-join ignore ","))))

(defun gnome-proxy--enabled? ()
  "Check if proxy is enabled."
  (string= "manual" (gsettings-get "org.gnome.system.proxy" "mode")))

(defun gnome-proxy--set-env (var scheme)
  "Set the environemt VAR variable based on proxy settings for SCHEME."
  (let* ((var (format "%s" var))
         (scheme (format "%s" scheme)))
    (when-let ((host (gnome-proxy--get-host scheme))
               (port (gnome-proxy--get-port scheme)))
      (dolist (v (list (downcase var) (upcase var)))
        (setenv v (format "%s://%s:%s" scheme host port))))))

(defun gnome-proxy--set-no-proxy ()
  "set the NO_PROXY variable."
  (let ((ignore (gnome-proxy--get-ignore)))
    (setenv "no_proxy" ignore)
    (setenv "NO_PROXY" ignore)))

(defun gnome-proxy--unset-env (&rest vars)
  "Unset all environment variables VARS."
  (let ((vars (mapcar (lambda (x) (format "%s" x)) vars)))
    (dolist (v (append vars (mapcar #'upcase vars)))
      (setenv v))))

;;;###autoload
(defun gnome-proxy-setup-env (&rest _)
  "Set environment variables accordingly to system settings.
Aqcuires proxy information from the gsettings interface and set
the respecting environment variables."
  (interactive)
  (if (not (gnome-proxy--enabled?))
      (gnome-proxy--unset-env 'http_proxy 'https_proxy 'ftp_proxy 'no_proxy)
    (gnome-proxy--set-env 'http_proxy 'http)
    (gnome-proxy--set-env 'https_proxy 'https)
    (gnome-proxy--set-env 'ftp_proxy 'ftp)
    (gnome-proxy--set-no-proxy)))

;;;###autoload
(define-minor-mode gnome-proxy-watch-mode
  ""
  :global t
  :lighter " DBUS Proxy Watcher"
  (let ((buffer " *dbus-proxy-watcher*"))
    (gnome-proxy-setup-env)
    (if gnome-proxy-watch-mode
        (make-process
         :name "dconf watch"
         :buffer " *dbus-proxy-watcher*"
         :command '("dconf" "watch" "/system/proxy/mode")
         :connection-type 'pipe
         :filter #'gnome-proxy-setup-env
         :noquery t)
      (when-let ((buffer (get-buffer " *dbus-proxy-watcher*")))
        (delete-process (get-buffer-process buffer))
        (kill-buffer buffer)))))

(provide 'gnome-proxy)
;;; gnome-proxy.el ends here
